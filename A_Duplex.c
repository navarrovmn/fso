#define _POSIX_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <poll.h>
#include <signal.h>

#define CHILD 0
#define ERROR -1

typedef struct _msgbuf {
	long int mtype;
	char mtext[1024];
} msgbuf;

struct pollfd _poll = { STDIN_FILENO, POLLIN|POLLPRI, 0 };

int main(int argc, char * argv[]){
	int spid, msqid, A_socketfd;
	key_t key;
	msgbuf in_buf, out_buf;
	struct sockaddr_in B_addr;

	if(argc < 3){
		printf("Numero de ip ou porta nao fornecidos, o programa ira encerrar...\n");
		exit(1);
	}

	srand(time(NULL));
	if((key = ftok("A.c", rand())) == -1){
		perror("ftok");
		exit(1);
	}

	if((msqid=msgget(key, 0666 | IPC_CREAT)) < 0){
		perror("msgget");
		exit(1);
	}

	memset((char *)&B_addr,0,sizeof(B_addr));
	B_addr.sin_family = AF_INET;
	B_addr.sin_addr.s_addr = inet_addr(argv[1]);
	B_addr.sin_port = htons(atoi(argv[2]));

	if((A_socketfd = socket(PF_INET, SOCK_STREAM, 0)) < 0 ){
		perror("socket");
		exit(1);
	}

	if(connect(A_socketfd, (struct sockaddr *) &B_addr, sizeof(struct sockaddr)) < 0){
		printf("Erro: B nao esta conectado\n");
		exit(1);
	}

	fcntl(A_socketfd, F_SETFL, fcntl(A_socketfd, F_GETFL, 0) | O_NONBLOCK);

	spid = fork();

	if(spid == CHILD){
		printf("Conexão realizada com sucesso\n");

		while(1){
			if(msgrcv(msqid, &in_buf, sizeof(in_buf), 1, MSG_NOERROR | IPC_NOWAIT) != -1){
				send(A_socketfd, &in_buf, sizeof(in_buf), 0);
				if(!strcmp(in_buf.mtext, "FIM")){
					break;
				}
			}else{
				if(recv(A_socketfd, &out_buf, sizeof(out_buf), 0) > 0){
					out_buf.mtype = 2;
					if(!strcmp(out_buf.mtext, "FIM")){
						break;
					}
					if(msgsnd(msqid, &out_buf, sizeof(out_buf), 0) == -1){
						perror("msgsnd");
						exit(1);
					}
				}
			}
		}
		close(A_socketfd);
		kill(getppid(), SIGKILL);
		exit(0);
	}

	else if(spid == ERROR){
		perror("fork");
		exit(1);
	}

	else{
		while(1){
			if(poll(&_poll, 1, 100)){
				fgets(in_buf.mtext, sizeof(in_buf.mtext), stdin);
				in_buf.mtext[strlen(in_buf.mtext)-1] = 0;

				in_buf.mtype = 1;
				if(msgsnd(msqid, &in_buf, sizeof(in_buf), 0) == -1){
					perror("msgsnd");
					exit(1);
				}
			}else{
				if(msgrcv(msqid, &out_buf, sizeof(out_buf), 2, MSG_NOERROR | IPC_NOWAIT) != -1){
					printf("\tMensagem recebida >> %s\n", out_buf.mtext);
				}
			}
		}
	}

	return 0;
}
