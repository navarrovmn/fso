#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <string.h>
#include <time.h>

#define CHILD 0
#define ERROR -1

typedef struct _msgbuf {
	long int mtype;
	char mtext[128];
} msgbuf;

int main (){
	msgbuf buf; 
	int msqid;
	int shm_id;
	struct msqid_ds msgq;
	key_t key;
	pid_t spid;
	void * shm_address;

	srand(time(NULL));
	if ((key = ftok("A.c", rand())) == -1){
		perror("ftok");
		exit(1);
	}

	if ((msqid = msgget(key, 0666 | IPC_CREAT)) == -1){
		perror("msgget");
		exit(1);
	}

	spid = fork();

	if(spid == CHILD){
		key = ftok("A.c", 0xf2);
		if((shm_id = shmget(key, sizeof(buf) + 1, 0666 | IPC_CREAT)) == -1){
			perror("shmget");
			exit(1);
		}

		if((shm_address = shmat(shm_id, NULL, 0)) == (void *) -1){
			perror("shmat");
			exit(1);
		}
		*((char*)shm_address) = 1;

		while(1){		
			do{
				msgctl(msqid, IPC_STAT, &msgq);
			}while( *((char*)shm_address) == 0 || msgq.msg_qnum == 0);
      
	    		if(msgrcv(msqid, &buf, sizeof(buf), 0, 0) == -1){
				perror("msgrcv");
				exit(1);
			}
	    	strcpy((char *)shm_address + 1, buf.mtext);
			*((char*)shm_address) = 0;
		}
	}
	else if(spid == ERROR){
		perror("fork");
		exit(1);
	}
	else{
		while(fgets(buf.mtext, 128, stdin) != NULL){
			buf.mtype = 1;
			if(msgsnd(msqid, &buf, sizeof(buf), 0) == -1){
				perror("msgsnd");
			}
		}
	}

	return 0;
}
