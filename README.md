# Comunicação entre Processsos

Projeto desenvolvido para a disciplina de Fundamentos dos Sistemas Operacionais da Universidade de Brasília, que tem como objetivo de entender como os principais mecanismos de comunicação POSIX funcionam e se comportam.

## Execução

Para a execução da versão final que utiliza de sockets para simular uma conversa bidirecional basta rodar o `make`, e depois o `makerun`.
De qualquer maneira é possível compilar e rodar as outras versões para ver o progresso do código e o funcionamento da memória compartilhada.
Aconselha-se compilar da seguinte maneira: `gcc -o <nome_do_executável> -ansi -pedantic -D_SVID_SOURCE -W -Wall <nome_do_arquivo>.c`

