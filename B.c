#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <string.h>
#include <time.h>

#define CHILD 0
#define ERROR -1

typedef struct _msgbuf {
	long int mtype;
	char mtext[128];
} msgbuf;

int main(){
	int spid, msqid;
	key_t key;
	msgbuf buf;
	struct msqid_ds msgq;
	int shm_id;
	void * shm_address;

	srand(time(NULL));
	key = ftok("B.c", rand());

	if((msqid=msgget(key, 0666 | IPC_CREAT)) == -1){
		perror("msgget\n");
		exit(1);
	}	

	spid = fork();
	
	
	if(spid == CHILD){
		key = ftok("A.c", 0xf2);
		if((shm_id = shmget(key, sizeof(buf)+1, 0666)) == -1){
			perror("shmget");
			exit(1);
		}

		if((shm_address = shmat(shm_id, NULL, 0)) == (void *) -1){
			perror("shmat");
			exit(1);
		}

		*((char*)shm_address) = 1;

      		while(1){
        		while( *((char*)shm_address) == 1);
             
		    	strcpy(buf.mtext, (char *) shm_address + 1);

        		*((char*)shm_address) = 1;

		    	buf.mtype = 1;
			if(msgsnd(msqid, &buf, sizeof(buf), 0) == -1){
				perror("msgsnd");
				exit(1);
			}
		}			
	}
	else if(spid == ERROR){
		perror("fork\n");
		exit(1);
	}
	else{
		while(1){	
			do{
				msgctl(msqid, IPC_STAT, &msgq);
			}while(msgq.msg_qnum == 0);
	      
		    	if(msgrcv(msqid, &buf, sizeof(buf), 0, 0) == -1){
				perror("msgrcv");
				exit(1);
			}

			printf("> %s", buf.mtext);
		}
	}

	return 0;
}
