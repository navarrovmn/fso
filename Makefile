RANDINT:=$(shell python -c 'from random import randint; print(randint(1024, 4096));')

all: 
	gcc -o A -ansi -pedantic -D_SVID_SOURCE -W -Wall A_Duplex.c
	gcc -o B -ansi -pedantic -D_SVID_SOURCE -W -Wall B_Duplex.c
	@echo "\nPara rodar, execute './A IP PORTA' e './B IP PORTA' ou use 'make run'"

run:
	gnome-terminal -e "./A 127.0.0.1 $(RANDINT)" &
	./B 127.0.0.1 $(RANDINT)

