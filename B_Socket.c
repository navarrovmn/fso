#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>

#define CHILD 0
#define ERROR -1

typedef struct _msgbuf {
	long int mtype;
	char mtext[128];
} msgbuf;

int main (int argc, char *argv[]){
	msgbuf buf;
	int msqid, B_socketfd, A_socketfd;
	struct msqid_ds msgq;
	key_t key;
	pid_t spid;
	struct sockaddr_in B_addr, A_addr;
	socklen_t A_addrlen;

	if(argc < 3){
		printf("Numero de ip ou porta nao fornecidos, o programa ira encerrar...\n");
		exit(1);
	}

	srand(time(NULL));
	if ((key = ftok("B.c", rand()))  == -1){
		perror("ftok");
		exit(1);
	}

	if ((msqid = msgget(key, 0666 | IPC_CREAT)) < 0){
		perror("msgget");
		exit(1);
	}

	memset((char *)&B_addr,0,sizeof(B_addr));
	B_addr.sin_family = AF_INET;
	B_addr.sin_addr.s_addr = inet_addr(argv[1]);
	B_addr.sin_port = htons(atoi(argv[2]));


	if((B_socketfd = socket(PF_INET, SOCK_STREAM, 0)) < 0){
		perror("socket");
		exit(1);
	}

	if(bind(B_socketfd, (struct sockaddr *) &B_addr, sizeof(B_addr)) < 0){
		perror("bind");
		exit(1);
	} 

	if (listen(B_socketfd, 2) < 0) {
		perror("listen");
		exit(1);
	}

	while(1){
		A_addrlen = sizeof(A_addr);

		printf("Conectando...\n");
		if((A_socketfd = accept(B_socketfd,(struct sockaddr *) &A_addr, &A_addrlen)) < 0){
			perror("accept");
			exit(1);
		}

		spid = fork();

		if(spid == CHILD){
			close(B_socketfd);
			printf("Cliente conectado com sucesso\n");

			while(1){
				read(A_socketfd, &buf, sizeof(buf));

				buf.mtype = 1;
				if(msgsnd(msqid, &buf, sizeof(buf), 0) < 0){
					perror("msgsnd");
					exit(1);
				}
			}
			exit(0);
		}

		else if(spid == ERROR){
			perror("fork");
			exit(1);
		}

		else{
			while(1){
				do{
					msgctl(msqid, IPC_STAT, &msgq);
				}while(msgq.msg_qnum == 0);

				if(msgrcv(msqid, &buf, sizeof(buf), 0, 0) == -1){
					perror("msgrcv");
				}

				printf("> %s\n", buf.mtext);
			}
		}
	}

	return 0;
}
