#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/shm.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define CHILD 0
#define ERROR -1

typedef struct _msgbuf {
	long int mtype;
	char mtext[128];
} msgbuf;

int main(int argc, char * argv[]){
	int spid, msqid, A_socketfd;
	key_t key;
	msgbuf buf;
	struct msqid_ds msgq;

	struct sockaddr_in B_addr;

	if(argc < 3){
		printf("Numero de ip ou porta nao fornecidos, o programa ira encerrar...\n");
		exit(1);
	}

	srand(time(NULL));
	if((key = ftok("A.c", rand())) == -1){
		perror("ftok");
		exit(1);
	}

	if((msqid=msgget(key, 0666 | IPC_CREAT)) < 0){
		perror("msgget");
		exit(1);
	}

	memset((char *)&B_addr,0,sizeof(B_addr));
	B_addr.sin_family = AF_INET;
	B_addr.sin_addr.s_addr = inet_addr(argv[1]);
	B_addr.sin_port = htons(atoi(argv[2]));

	spid = fork();

	if(spid == CHILD){
		if((A_socketfd = socket(PF_INET, SOCK_STREAM, 0)) < 0 ){
			perror("socket");
			exit(1);
		}

		if(connect(A_socketfd, (struct sockaddr *) &B_addr, sizeof(struct sockaddr)) < 0){
			perror("connect");
			exit(1);
		}


		while(1){
			do{
				msgctl(msqid, IPC_STAT, &msgq);
			}while(msgq.msg_qnum == 0);

		  if(msgrcv(msqid, &buf, sizeof(buf), 0, 0) < 0){
				perror("msgrcv");
				exit(1);
			}

			send(A_socketfd, &buf, sizeof(buf), 0);
		}
	}
	else if(spid == ERROR){
		perror("fork");
		exit(1);
	}
	else{
		while(fgets(buf.mtext, sizeof(buf.mtext), stdin) != NULL){

			buf.mtype = 1;
		  if(msgsnd(msqid, &buf, sizeof(buf), 0) < 0){
				perror("msgrcv");
				exit(1);
			}
		}
	}

	return 0;
}
